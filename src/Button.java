import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;


public class Button {
	public float xPos;
	public float yPos;
	public float xSize;
	public float ySize;
	public Shape Shape;
	public Button above;
	public Button below;
	
	
	Shape shape;
	Button(float xPos, float yPos, float xSize, float ySize){
		this.xPos=xPos;
		this.yPos=yPos;
		this.xSize=xSize;
		this.ySize=ySize;
		shape=new Rectangle(xPos, yPos, xSize, ySize);
		
	}

	public boolean hasBelow(){
		if (below!=null){
			return true;
		}
		return false;
	}
	
	public boolean hasAbove(){
		if (above!=null){
			return true;
		}
		return false;
	}
}
