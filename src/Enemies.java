import java.util.ArrayList;
import java.util.Random;
/**
 * Class that handles the enemy creation and enemy movement.
 * @author Axel Karlsson, Jonathan Wahlund, Filip Reinholdsson
 *
 */
public class Enemies {
	ArrayList<Enemy> enemies = new ArrayList<Enemy>(); // Array that holds the enemies.
	
	boolean movingRight=true;	// True when moving right.
	boolean moveDown=false;		// True when moving left.
	
	// Enemy movement settings.
	int speedX=2;				// How many pixels enemies will move in x-axis.
	int speedY=5;				// How many pixels enemies will move in y-axis.
	int flankX=5;				// How close the enemies can get to a wall on the x-axis.
	int velocityX = speedX;		// Enemies current speed and direction in x-axis.
	
	int sizeEnemy = 15;			// Size of enemies created.
	int sizeBoss = 148/2;
	int enemiesX=10;
	int enemiesY=5;
	int spaceBetweenEnemies=15;
	int enemyHealth=1;
	int waveIndex=0;
	
	// Bullet settings.
	int enemyBulletSize=8;
	int enemyBulletSpeed=5;
	
	String waveInfo;
	
	/**
	 * Constructor for enemies, calls Enemy class.
	 */
	Enemies(){
	}
	
	/**
	 * Moves all the enemies. If enemies are near the edge of window, moves them downwards.
	 */
	public void move(){
		for(Enemy e1: enemies){
			if(e1.shape.getMinX()<(flankX+1)||e1.shape.getMaxX()>(799-flankX)){	// Check if enemies are near the edge.
				for(Enemy e2: enemies){
					if(waveIndex%4==0){
						e2.shape.setY(e2.shape.getMinY()+speedY*4);
					}else{
						e2.shape.setY(e2.shape.getMinY()+speedY);	// Moves enemies downward.
					}
				}
				movingRight=!movingRight;	// Change direction.
				if(movingRight){			// Set direction.
					velocityX=speedX;
				}
				else{
					velocityX=-speedX;
				}
				for(Enemy e: enemies){		
					e.shape.setX(e.shape.getMinX()+velocityX/2);	// Move enemies.
				}
				break;
			}
		}		
		for(Enemy e: enemies){		
			e.shape.setX(e.shape.getMinX()+velocityX);	// Move enemies.
		}
	}
	
	/**
	 * Selects a random enemy that shoots.
	 * Makes sure that the selected enemy doesn't have any enemies below it
	 * @return projectile object at the location of the selected enemy
	 */
	public Projectile shoot(){
		Random randomGenerator = new Random();
		Enemy shooter = enemies.get(randomGenerator.nextInt(enemies.size()));
		for(Enemy e: enemies){
			if(e.shape.getMaxX()==shooter.shape.getMaxX()){
				if(e.shape.getMaxY()>shooter.shape.getMaxY()){
					shooter=e;
				}
			}
		}
		return shooter.shoot(enemyBulletSpeed,enemyBulletSize);
	}
	
	public void spawnEnemies(int waveIndex){
		if(waveIndex%4==0){	// Boss fight.
			enemyHealth=29+waveIndex/2;
			waveInfo="Boss with " + enemyHealth + " hp.";
			int xPos=400;
			int yPos=50;
			enemies.add(new Enemy(xPos, yPos+(sizeBoss/2), sizeBoss, enemyHealth, 2));
		}else{
			enemyHealth=1+waveIndex/4;
			
			waveInfo="Enemy invaders with " + enemyHealth + " hp.";
			for(int yPos=spaceBetweenEnemies+sizeEnemy;yPos<((enemiesY)*spaceBetweenEnemies+enemiesY*2*sizeEnemy)&&yPos<(400-sizeEnemy);yPos=yPos+2*sizeEnemy+spaceBetweenEnemies){
				for(int xPos=spaceBetweenEnemies+sizeEnemy;xPos<((enemiesX)*spaceBetweenEnemies+enemiesX*2*sizeEnemy)&&xPos<(700-sizeEnemy);xPos=xPos+2*sizeEnemy+spaceBetweenEnemies){
					enemies.add(new Enemy(xPos, yPos, sizeEnemy, enemyHealth, 1));
				}
			}
		}
	}
	
	public String getWaveInfo(){
		return waveInfo;
	}
	
	public int getEnemyHealth(){
		return enemyHealth;
	}
}
