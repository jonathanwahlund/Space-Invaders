import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;


/**
 * Creates enemy object.
 * @author Axel Karlsson, Jonathan Wahlund, Filip Reinholdsson
 *
 */
public class Enemy{
	Shape shape;
	int startingHealth;
	
	int xPos;	// x position of an enemy.
	int yPos;	// y position of an enemy.
	int size;	// The size of an enemy.
	int health;
	int damage;
	
	/**
	 * Constructor for enemy objects
	 * @param xPos
	 * @param yPos
	 */
	Enemy(int xPos, int yPos, int size, int health, int damage){	
		this.xPos=xPos;
		this.yPos=yPos;
		this.size=size;
		this.startingHealth=health;
		this.health=startingHealth;
		this.damage=damage;
		shape = new Circle(xPos, yPos, size);	// Creates enemy at given coordinates.
	}
	
	/**
	 * Returns the size of the enemy.
	 * @return
	 */
	public int getSize(){
		return size;
	}
	
	public Projectile shoot(int pSpeed, int pSize){
		Projectile p= new Projectile((int)shape.getCenterX(), (int)shape.getMaxY(), pSpeed, pSize, false, damage);
		return p;
	}
	
	public int damage(){
		health--;
		return health;
	}
	
	public int damage(int dmg){
		health-=dmg;
		return health;
	}
	
	public int getHealth(){
		return health;
	}
	
	public int getStartingHealth(){
		return startingHealth;
	}
	
	public void setDamage(int dmg){
		damage=dmg;
	}
	
	public int getDamage(){
		return damage;
	}
}
	
	
	
	

