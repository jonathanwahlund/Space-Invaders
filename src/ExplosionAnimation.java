import org.newdawn.slick.Animation;

/**
 * 
 */

/**
 * @author Jonathan
 *
 */
public class ExplosionAnimation {
	float yPos;
	float xPos;
	int duration;
	Animation explosion; 
	long timeAlive=0;
	ExplosionAnimation(float f, float g, int duration){
		this.yPos=g;
		this.xPos=f;
		this.duration=duration;
		explosion = new Animation(Textures.explosion1ss, duration/15);
		explosion.setLooping(false);
	}
	
	public float getYPos(){
		return yPos;
	}
	
	public float getXPos(){
		return xPos;
	}
	
	public long tick(long delta){
		explosion.update(delta);
		timeAlive+=delta;
		return timeAlive;
	}
	
	public void draw(){
		explosion.draw(xPos,yPos);
	}
	
	public long getDuration(){
		return duration;
	}
}
