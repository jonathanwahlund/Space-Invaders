import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * @author Jonathan Wahlund, Axel Karlsson, Filip Reinholdsson
 */
public class GameOver extends BasicGameState{
	public String message = "Game Over!";
	ArrayList<HighScore> highscores = new ArrayList<HighScore>(); // Array that holds the highscores.
	TextField textField;
	boolean scoreSaved=false;
	String defaultText="";
	long timeDelay=5000;
	
	@Override
	public void enter(GameContainer c, StateBasedGame s) {
			Sounds.gameOverMusic.loop();
	}
	
	@Override
	public void leave(GameContainer c, StateBasedGame s) {
		Sounds.gameOverMusic.stop();
	}
	
	@Override
	public void init(GameContainer c, StateBasedGame s) throws SlickException {
		try {
			File myFile = new File("highscores.txt");
			myFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			importHighscores();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		
		textField = new TextField(c, c.getDefaultFont(),200, 300, 400, 20);
	    textField.setText(defaultText);
	    textField.setBorderColor(new Color(255,255,255));
	    textField.setFocus(true);
	}

	@Override
	public void render(GameContainer c, StateBasedGame s, Graphics g) throws SlickException {
		//g.drawString(message, 340, 50);
		g.drawString("Your score was "+ SpaceInvaders.getScore(), 320, 150);
		g.drawString("Press right Ctrl to play again!", 280, 200);
		
		if(!scoreSaved){
			g.drawString("Enter name and press enter to save your score locally", 180, 275);
			textField.render(c, g);
		}
		g.drawString("Local high scores:", 250, 365);
		g.drawString("Rank    Score    Name", 250, 380);
		int rank=1;
		int yPos=400;
		g.drawImage(Textures.gameOver, 0, 0);
		if(highscores.isEmpty()){
			g.drawString("No high scores saved locally.",250,yPos);
		}else{
			int counter=0;
			for(HighScore hs: highscores.subList(0,highscores.size())){
				String thisScore=String.valueOf(hs.getScore());
				String thisSeperater="        ";
				if(thisScore.length()>8){
					thisScore="Too high";
				}
				g.drawString(rank + ":      " + thisScore + thisSeperater.substring(0,(9-thisScore.length())) + hs.getName().trim(),250,yPos);
				rank++;
				yPos+=20;
				if(counter>=8){
					break;
				}
				counter++;
			}
		}
	}

	@Override
	public void update(GameContainer c, StateBasedGame s, int delta) throws SlickException {
	
		textField.setFocus(true);
		Input input = c.getInput();
		if(input.isKeyDown(Input.KEY_RCONTROL)){
			c.reinit();
			s.enterState(0);
		}
		if(input.isKeyDown(Input.KEY_ENTER)&&!scoreSaved){
			saveScore();
			scoreSaved=true;
			try {
				exportHighscores();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(input.isKeyDown(Input.KEY_ESCAPE)){		// Moves left if possible if left arrow key is pressed.
			s.enterState(2);
		}
	}

	@Override
	public int getID() {
		return 1;
	}
	
	public void exportHighscores() throws IOException{
		FileOutputStream fos = new FileOutputStream("highscores.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(highscores);
		oos.close();
	}
	
	public void importHighscores() throws IOException, ClassNotFoundException{
		FileInputStream fis = new FileInputStream("highscores.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		ArrayList<HighScore> arrayList = (ArrayList<HighScore>) ois.readObject();
		highscores = arrayList;
		ois.close();
	}
	
	public void saveScore(){
		HighScore newHighScore=new HighScore(textField.getText(),SpaceInvaders.getScore());
		if(highscores.isEmpty()){
			highscores.add(newHighScore);
			scoreSaved=true;
		}else{
			int index=0;
			int newScore=newHighScore.getScore();
			for(HighScore hs: highscores){
				if(newScore>hs.getScore()&&!scoreSaved){
					highscores.add(index, newHighScore);
					if(highscores.size()>50){
						highscores.remove(50);
					}
					scoreSaved=true;
					break;
				}
				index++;
			}
			if(!scoreSaved&&highscores.size()<50){
				highscores.add(newHighScore);
				scoreSaved=true;
			}
		}
		scoreSaved=true;
	}
}
