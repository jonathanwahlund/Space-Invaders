import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * 
 * @author Jonathan Wahlund, Axel Karlsson, Filip Reinholdsson
 *
 */
public class GameSetup extends StateBasedGame{
	
	
	
	public final static String VERSION = "1.0"; 	// This variable hold the version of the game.
	public final static int maxFPS = 60; 			// This variable holds the max allowed FPS.
	public static boolean musicOn = true;
	
	public GameSetup(String name){
		super(name);
	}

	/**
	 * Creates a 800x600 resolution window.
     * @param args
	 * @throws SlickException 
     */
    public static void main(String[] args) throws SlickException {
		Sounds.initialize();
        AppGameContainer app = new AppGameContainer(new GameSetup("Space Invaders - Version " + VERSION));
        app.setDisplayMode(800, 600, false);	// Set resolution and windows size of the game.
        app.setTargetFrameRate(maxFPS);			// Configure the max-fps of the game.
        app.setShowFPS(false);					// Hide the fps-graph.
        app.start();							// Starts the program.
    }

	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		Textures.initialize();
		this.addState(new MainMenu());
		this.addState(new SpaceInvaders());
		this.addState(new GameOver());
	}
}
