import java.io.Serializable;

/**
 * @author Jonathan Wahlund
 */
public class HighScore implements Serializable{
	String name;	// Stores the name of the player.
	int score;		// Stores the highscore.
	HighScore(String name, int score){
		this.score=score;
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
	
	public int getScore(){
		return score;
	}
}
