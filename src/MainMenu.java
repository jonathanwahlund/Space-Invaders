import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


public class MainMenu extends BasicGameState{

	private Button playButton;
	private Button exitButton;
	private Button selectedButton;
	private float counter=0;
	private float buttonUpdateTime=5;

		
		@Override
		public void init(GameContainer c, StateBasedGame s)
				throws SlickException {
			playButton=new Button(350, 300, 100, 30);
			exitButton=new Button(350, 350, 100, 30);
			playButton.below = exitButton;
			exitButton.above = playButton;
			selectedButton=playButton;
		}

		@Override
		public void render(GameContainer c, StateBasedGame s, Graphics g)
				throws SlickException {
			g.drawImage(Textures.mainMenu, 0, 0);
			g.drawString("Play", 370, 305);
			g.drawString("Exit", 370, 355);
			g.drawImage(Textures.selection, selectedButton.shape.getMinX(), selectedButton.shape.getMinY());
		}
		
		@Override
		public void enter(GameContainer c, StateBasedGame s) {
				Sounds.mainMenuMusic.loop(1, 0.5f);
		}
		
		@Override
		public void leave(GameContainer c, StateBasedGame s) {
			Sounds.mainMenuMusic.stop();
		}

		@Override
		public void update(GameContainer c, StateBasedGame s, int delta)
				throws SlickException {
			counter+=delta;
			Input input = c.getInput();
			if(counter>=buttonUpdateTime){
				if(input.isKeyPressed(Input.KEY_DOWN)){
					if(selectedButton.hasBelow()){
						selectedButton=selectedButton.below;
					}
				}
				else if(input.isKeyPressed(Input.KEY_UP)){
					if(selectedButton.hasAbove()){
						selectedButton=selectedButton.above;
					}
					
				}
				if(input.isKeyPressed(Input.KEY_ENTER)){
					if(selectedButton==playButton){
						c.reinit();
						s.enterState(0);
					}
					if(selectedButton==exitButton){
						c.exit();
					}
				}
				if(input.isKeyPressed(Input.KEY_RCONTROL)){
					c.reinit();
					s.enterState(0);
				}
				
			}
			
			
		}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 2;
	}
	
}
