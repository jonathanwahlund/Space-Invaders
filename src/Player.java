
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

/**
 * Create player object, the ship.
 * @author Axel Karlsson, Jonathan Wahlund, Filip Reinholdsson
 *
 */
public class Player {
	Shape shape;	// Field for player shape.
	int velocity=4;	// Field for player velocity.
	static int xPos;		// Field for players x coordinate.
	static int yPos;		// Field for players y coordinate.
	
	int health=1;
	int damage=1;
	
	
	Player() throws SlickException{
		xPos=375;									// Middle of the x-axis with resolution 800x600.
		yPos=550;									// Down on screen with resolution 800x600.
		shape=new Rectangle(xPos, yPos, 50, 50);	// create player shape at bottom centre of screen.	(Used by the game engine to know where the player is and the size of the hitbox)
	}
	
	/**
	 * Moves player to the left.
	 * Method is called when Left Key is pressed
	 */
	public void moveLeft(){
		xPos-=velocity;		// Subtract velocity from x Position.
		shape.setX(xPos);	// Set new x Position.
	}
	
	/**
	 * Moves player to the right
	 * Method is called when Left Key is pressed
	 */
	public void moveRight(){
		xPos+=velocity;		// Add velocity to x Position.
		shape.setX(xPos);	// Set new x Position.
	}
	
	public Projectile shoot(int xPosOffset){
		Projectile p= new Projectile((int)shape.getCenterX()+xPosOffset, yPos, 8, 8, true, damage);
		return p;
	}
	
	public int damage(int dmg){
		health-=dmg;
		return health;
	}
	
	public int getHealth(){
		return health;
	}
	
	public void setDamage(int dmg){
		damage=dmg;
	}
	
	public int getDamage(){
		return damage;
	}
	
	public void changeDamage(int dmg){
		damage+=dmg;
	}
}
