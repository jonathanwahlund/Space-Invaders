import java.util.Random;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

/**
 * @author Jonathan Wahlund, Axel Karlsson, Filip Reinholdsson
 * Creates a power up related to the index.
 */
public class PowerUp {
	Shape shape;
	Image icon;
	int typeIndex;
	int velocity=1;
	int size;
	int yPos=50;
	int xPos=-size*2;
	long timeActivated;
	String powerUpName;
	boolean alive=true;
	boolean activated=false;

	PowerUp(int typeIndex) throws SlickException{
		this.typeIndex=typeIndex;
		setRandomStartPos();
		switch (typeIndex){
		case 0: //Rapid Fire
			shape = new Circle(xPos, yPos, 20);
			icon = new Image ("/res/rapidfire.png");
			break;
		case 1: //Double barrel
			shape = new Rectangle(xPos-20, yPos-20, 40, 20);
			icon = new Image ("/res/DOUBLEBARREL.png");
			break;
		case 2: //+1 health
			shape = new Rectangle(xPos-20, yPos-20, 20, 70);
			icon = new Image ("/res/hp.png");
			break;
		case 3: //Explosive bullets
			shape = new Rectangle(xPos-20, yPos-20, 40, 50);
			icon = new Image ("/res/explosive.png");
			break;
		}
	}
	
	private void setRandomStartPos(){
		Random randomGenerator = new Random();
		if(randomGenerator.nextBoolean()){
			velocity*=-1;
			xPos=800-xPos;
		}
	}
	
	public void move(){
		shape.setCenterX(shape.getCenterX()+velocity);
	}
	
	public int getTypeIndex(){
		return typeIndex;
	}
	
	public int activate(){
		timeActivated=System.nanoTime()/1000000;
		activated=true;
		return typeIndex;
	}
	
	public long getTimeActivated(){
		return timeActivated;
	}
	
	public void setPowerUpName(String newName){
		powerUpName=newName;
	}
	
	public String getPowerUpName(){
		return powerUpName;
	}
	
	public void destroy(){
		alive=false;
	}
	
	public boolean isAlive(){
		return alive;
	}
	
	public boolean isActivated(){
		return activated;
	}
}
