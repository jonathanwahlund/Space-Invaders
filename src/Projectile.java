import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

/**
 * @author Jonathan Wahlund, Axel Karlsson, Filip Reinholdsson
 *
 */
public class Projectile {
	Shape shape;
	int speed;			// Projectile speed.
	int xPos;			// x position of an enemy.
	int yPos;			// y position of an enemy.
	int size;			// The size of an enemy.
	boolean alive=true;	// Should projectile be present on the screen?
	boolean fromPlayer;	// Is fired from player?
	int damage;
	
	Projectile(int xPos, int yPos, int speed, int size, boolean fromPlayer, int damage){
		this.speed=speed;
		this.xPos=xPos;
		this.yPos=yPos;
		this.size=size;
		this.fromPlayer=fromPlayer;
		this.damage=damage;
		shape = new Circle(xPos, yPos, size);	// Creates enemy at given coordinates.
	}
	
	/**
	 * Moves the projectile up if fired by player, or down if fired by enemy.
	 */
	public void move(){
		if(fromPlayer){
			shape.setCenterY(shape.getCenterY()-speed);
		}else{
			shape.setCenterY(shape.getCenterY()+speed);
		}
	}
	
	public boolean getFromPlayer(){
		return fromPlayer;
	}
	
	public boolean isAlive(){
		return alive;
	}
	
	public void destroy(){
		alive=false;
	}
	
	public void setDamage(int dmg){
		damage=dmg;
	}
	
	public int getDamage(){
		return damage;
	}
}
