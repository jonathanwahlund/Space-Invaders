
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 * @author Jonathan Wahlund, Axel Karlsson, Filip Reinholdsson
 * Handles the game sound.
 */
public class Sounds {
	static Music backgroundMusic = null;
	static Music mainMenuMusic = null;
	static Music gameOverMusic = null;
	
	static Sound pang = null;
	static Sound boom = null;
	static Sound powerUp = null;
	
	
	public static void initialize() throws SlickException{
		backgroundMusic = new Music("res/stardstm.ogg"); // Creates the music object from file
		mainMenuMusic = new Music("res/mainmenu.ogg");
		gameOverMusic = new Music("res/gameover.ogg");
		
		pang = new Sound("res/pang.ogg");
		boom = new Sound("res/boom.ogg");
		powerUp = new Sound("res/powerup.ogg");
	}
	
	

}
