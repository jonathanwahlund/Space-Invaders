import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


/**
 * @author Jonathan Wahlund, Axel Karlsson, Filip Reinholdsson
 *
 * This is the main class of this program.
 */
public class SpaceInvaders extends BasicGameState{
	
	ArrayList<Projectile> projectiles = new ArrayList<Projectile>(); // Array that holds the projectiles.
	ArrayList<PowerUp> powerUps = new ArrayList<PowerUp>(); // Array that holds the power ups.
	
	ArrayList<Projectile> projectilesToRemove = new ArrayList<Projectile>(); // Array that holds the projectiles.
	ArrayList<Enemy> enemiesToRemove = new ArrayList<Enemy>(); // Array that holds the enemies to be removed.
	ArrayList<PowerUp> powerUpsToRemove = new ArrayList<PowerUp>(); // Array that holds the powerups to be removed.
	ArrayList<PowerUp> powerUpsToDeactivate = new ArrayList<PowerUp>(); // Array that holds the powerups to be deactivated.
	ArrayList<ExplosionAnimation> exploAnims = new ArrayList<ExplosionAnimation>(); // Array that holds the explosion animations.
	ArrayList<ExplosionAnimation> exploAnimsToRemove = new ArrayList<ExplosionAnimation>(); // Array that holds the explosion animations to be removed.
	
	
	static Player player; 				// Variable to store player.
	Enemies enemies;					// Variable to store enemies.
	
	// Counters the tracks time with delta.
	private long counterPlayer=0;		// Time counter for player.
	private long counterEnemy=0;		// Time counter for enemies.
	private long counterProjectile=0;	// Time counter for projectiles.
	private long counterPowerUp=0;		// Time counter for power up.
	private long counterOthers=0;
	
	// Update times, used with counters.
	private int playerUpdateTime=1;		// Time between player movement. Larger number = slower player.
	private int enemyUpdateTime=5;		// Time between enemy movement. Larger number = slower enemies.
	private int projectileUpdateTime=1;	// Time between projectile movement. Larger number = slower projectiles.
	private int powerUpUpdateTime=5;	// Time between power up movement. Larger number = slower power up.
	private int othersUpdateTime=5;
	
	// Holds the time when the given thing last was used.
	private long playerLastFired=0; 							// Player last fire.
	private long enemiesLastFired=System.nanoTime()/1000000;	// Enemies last fire.
	private long powerUpSpawnd=System.nanoTime()/1000000;		// Power up last spawnd.
	
	// The minimum milliseconds between events.
	private long timeBetweenPlayerFire = 1000;	// Player fire.
	private long timeBetweenEnemyFire = 500;	// Enemies fire.
	private long timeBetweenPowerUps=5000;		// Power Up spawn.
	private int timeExplotionAnimation=320;		// Time an explosion animation is present.
	
	private long timePoweredUp = 30000;	// The time in milliseconds the player is powered up.
	
	private static int score;			// Stores the players current score.		
	private int waveIndex=0;			// Wave counter.
	private String waveInfo;			// Variable holds information of current wave.
	private int playerBarrels=1;		// Stores value of amount of barrels.
	private int spaceBetweenBarrels=15;	// Space between bullets.
	private int maxHealth=3;			// Player max health.
	
		@Override
	public void enter(GameContainer c, StateBasedGame s) {
			Sounds.backgroundMusic.loop();	// Starts background music, will loop continuily.
	}
	
	@Override
	public void leave(GameContainer c, StateBasedGame s) {
		Sounds.backgroundMusic.stop();		// Stop backgoundmusic.
	}
	

	/**
     * This method is automatically called one time when the program is started.
     */
    @Override
	public void init(GameContainer c, StateBasedGame s) throws SlickException {
    	player = new Player();		// Creates the player object.
    	enemies = new Enemies();	// Creates the enemies object.
    	score=0; 					// Score-tracker is reset.
	}
    
    
    /**
     * Renders everything into the window.
     */
	@Override
	public void render(GameContainer c, StateBasedGame s, Graphics g) throws SlickException {
		if(Display.isActive()){				// Check if window is in focus of the user.
			g.setDrawMode(Graphics.MODE_NORMAL); //honors transparency
			Textures.background.draw(0,0);
			//g.draw(player.shape); 			// Draws the player. Only shape.
			g.drawImage(Textures.playerTexture, Player.xPos, Player.yPos); // Draws the player with texture.
			
			// Draws all the enemies.
			for(Enemy e: enemies.enemies){ 	
				//g.draw(e.shape);
				if(waveIndex%4==0){	// Boss fight.
					g.drawImage(Textures.bossTexture, e.shape.getMinX(), e.shape.getMinY());
				}else{
					g.drawImage(Textures.enemyTexture, e.shape.getMinX(), e.shape.getMinY());
				}
				
			}
			
			// Draws all the projectiles.
			for(Projectile p: projectiles){ // Draws all the projectiles.
				//g.draw(p.shape);
				if(p.getFromPlayer()){
					g.drawImage(Textures.bulletTexture, p.shape.getMinX(), p.shape.getMinY()); // Draws player projectiles.
				}else{
					g.drawImage(Textures.bulletTextureFlip, p.shape.getMinX(), p.shape.getMinY()); // Draws enemy projectiles.		
				}
				
			}
			g.drawString("Score: " + score,700,1);	// Draws the text displaying current score.
			
			// Draws the power up, if it exists.
			if(!powerUps.isEmpty()){			
				for(PowerUp p: powerUps){
					g.drawImage(p.icon,  p.shape.getMinX(), p.shape.getMinY());
				}
			}
			
			// Draws powered up text and time, if we are powered up.
			if(!powerUpsToRemove.isEmpty()){	
				int yPos=450;
				for(PowerUp p: powerUpsToRemove){
					if(p.getTypeIndex()!=2){
						g.drawString(p.getPowerUpName() + ": " + ((timePoweredUp+p.getTimeActivated()-System.nanoTime()/1000000)/1000) + " seconds left.",260,yPos);
						yPos+=15;
					}
				}
				
			}
			
			// Draws wave info and boss hp.
			if(!enemies.enemies.isEmpty()){		
				g.drawString(waveInfo,1,1);
				if(waveIndex%4==0){
					g.drawString("Boss current hp: " + enemies.enemies.get(0).getHealth(),1,15);
				}
			}
			
			// Draws player damage and attackspeed.
			g.drawString("Damage: " + player.getDamage(),1,539);
			g.drawString("Shots per second: " + round(((double) 1000/timeBetweenPlayerFire),2),1,554);
			
			// Draws player health in form of icons.
			for(int xPos=1;xPos<=maxHealth*33;xPos+=33){
				g.drawImage(Textures.emptyHealthIcon, xPos, 572);
			}
			for(int xPos=1;xPos<=player.getHealth()*33;xPos+=33){
				g.drawImage(Textures.healthIcon, xPos, 572);
			}
			
			// Draw exlosions (animations).
			for(ExplosionAnimation ani: exploAnims){
				ani.draw();
			}
		}
	}


	/**
	 * This method is called all the time the program running.
	 * It updates everything in the game, making the game run.
	 */
	@Override
	public void update(GameContainer c,StateBasedGame s, int delta) throws SlickException {
		
		if(Display.isActive()){		// Check if window is in focus of the user.
			// Update player.
			counterPlayer+=delta;	// Increase counters by delta (time).
			if (counterPlayer>playerUpdateTime){	// Checks player time-counter.
				Input input = c.getInput();
				if(input.isKeyDown(Input.KEY_LEFT)&&player.shape.getMinX()>1){		// Moves left if possible if left arrow key is pressed.
					player.moveLeft();
				}
				if(input.isKeyDown(Input.KEY_RIGHT)&&player.shape.getMaxX()<799){	// Moves right if possible if right arrow key is pressed.
					player.moveRight();
				}
				if(input.isKeyDown(Input.KEY_SPACE)&&(System.nanoTime()/1000000>(playerLastFired+timeBetweenPlayerFire))){	// Shoot if space is pressed.
									// Creates new projectile from player.
					if(playerBarrels==1){
						projectiles.add(player.shoot(0));
					}else{
						int xPosOffset=-(((playerBarrels-1)*spaceBetweenBarrels)/2);
						for(int i=0;i<playerBarrels;i++){
							projectiles.add(player.shoot(xPosOffset));
							xPosOffset+=spaceBetweenBarrels;
						}
					}
					Sounds.pang.play(1, 0.2f); // Weapon sound
					playerLastFired = System.nanoTime()/1000000;	// Updates last time player fired.
					//System.out.println(playerLastFired); // DEBUGG
				}
				if(input.isKeyDown(Input.KEY_ESCAPE)){		// Moves left if possible if left arrow key is pressed.
					s.enterState(2);
				}

				counterPlayer=0;	// Reset time-counter.
			}
			
			// Update other things.
			counterOthers+=delta;
			if(counterOthers>othersUpdateTime){
				if(enemies.enemies.isEmpty()){
					waveIndex++;
					enemies.spawnEnemies(waveIndex);
					waveInfo="Wave " + waveIndex + ": " + enemies.getWaveInfo();
				}
				counterOthers=0;
			}
			
			// Update enemies.
			counterEnemy+=delta;
			if(counterEnemy>enemyUpdateTime){	// Checks enemy time-counter.
				enemies.move();					// Moves the enemies.
				for(Enemy e: enemies.enemies){	// Check intersects for all enemies.
					if(e.shape.intersects(player.shape)||e.shape.getMaxY()>=600){
						s.enterState(1);		// Game Over.
					}
				}
				if(System.nanoTime()/1000000>(enemiesLastFired+timeBetweenEnemyFire)){
					projectiles.add(enemies.shoot());				// Create new projectile from enemy.
					enemiesLastFired = System.nanoTime()/1000000;	// Update last time enemies fired.
				}
				counterEnemy=0;		// Reset time-counter.
			}
			
			// Update power ups.
			counterPowerUp+=delta;
			if(counterPowerUp>powerUpUpdateTime){
				if(!powerUps.isEmpty()){
					for(PowerUp p: powerUps){
						p.move();
						if(p.shape.getCenterX()>850||p.shape.getCenterX()<-50){
							powerUpsToRemove.add(p);
						}
					}
				}
				if(System.nanoTime()/1000000>powerUpSpawnd+timeBetweenPowerUps){
					Random randomGenerator = new Random();
					if(waveIndex>3&&player.getDamage()<=enemies.getEnemyHealth()){
						int theRandom=randomGenerator.nextInt(4);
						if(theRandom==2&&player.getHealth()<3){
							theRandom=randomGenerator.nextInt(2);
						}
						powerUps.add(new PowerUp(theRandom));
					}else if(player.getHealth()<3){
						powerUps.add(new PowerUp(randomGenerator.nextInt(3)));
					}else{
						powerUps.add(new PowerUp(randomGenerator.nextInt(2)));
					}
					powerUpSpawnd=System.nanoTime()/1000000;
				}
				counterPowerUp=0;
			}
			
			// Update projectiles (bullets).
			counterProjectile+=delta;
			if(counterProjectile>projectileUpdateTime){
				for(Projectile p: projectiles){
					p.move();										// Move projectile.
					if(p.shape.getMaxY()<0||p.shape.getMinY()>800){	// Just removes the projectile if it is out of bounds.
						projectilesToRemove.add(p);					// Add projectile to be removed.
					} else if(p.getFromPlayer()){
						for(Projectile pO: projectiles){			// Hit other projectile.
							if(p.shape.intersects(pO.shape)&&p!=pO&&!pO.getFromPlayer()&&p.isAlive()&&pO.isAlive()){
								exploAnims.add(new ExplosionAnimation(p.shape.getCenterX()-16,p.shape.getMinY()-16,timeExplotionAnimation));
								p.destroy();						// This projectile is done. 
								pO.destroy();
								projectilesToRemove.add(p);		// Add to remove-list.
								projectilesToRemove.add(pO);
								break;
							}
						}
						if(p.isAlive()){								// Hit enemy.
							for(Enemy e: enemies.enemies){
								if(e.getHealth()>0&&p.shape.intersects(e.shape)&&p.isAlive()){
									if(e.damage(player.getDamage())<=0){
										score+=e.getStartingHealth();		// Increase score.
										enemiesToRemove.add(e);
										Sounds.boom.play(1, 0.2f); // Enemy destroyed sound
									}
									exploAnims.add(new ExplosionAnimation(p.shape.getCenterX()-16,p.shape.getMinY()-16,timeExplotionAnimation));
									p.destroy();					// This projectile is done. 
									projectilesToRemove.add(p);		// Add to remove-list.
									break;
								}
							}
						}
						if(p.isAlive()&&!powerUps.isEmpty()){						// Hit power up.
							for(PowerUp pU: powerUps){
								if(pU.isAlive()&&p.shape.intersects(pU.shape)){
									exploAnims.add(new ExplosionAnimation(p.shape.getCenterX()-16,p.shape.getMinY()-16,timeExplotionAnimation));
									Sounds.powerUp.play(1, 0.5f);  // Powerup sound
									pU.destroy();
									pU.setPowerUpName(activatePowerUp(pU.activate()));			// Call power up method.
									projectilesToRemove.add(p);				// Add to remove-list.
									powerUpsToRemove.add(pU);				// Clears the power up 
								}
							}
							
						}
					}else{
						if(p.isAlive()&&p.shape.intersects(player.shape)){
							exploAnims.add(new ExplosionAnimation(p.shape.getCenterX()-16,p.shape.getMaxY()-16,timeExplotionAnimation));
							player.damage(p.getDamage());
							p.destroy();					// This projectile is done. 
							projectilesToRemove.add(p);		// Add to remove-list.
							if(player.getHealth()<=0){
								s.enterState(1);	// Game Over.
							}
						}
					}
				}
				
				// Removes everything that should be removed.
				for(Projectile p: projectilesToRemove){
					projectiles.remove(p);		// Remove removed projectile.
				}
				projectilesToRemove.clear();
				for(Enemy e: enemiesToRemove){
					enemies.enemies.remove(e);
				}
				enemiesToRemove.clear();
				for(PowerUp p: powerUpsToRemove){
					powerUps.remove(p);
					if(p.getTypeIndex()==2||!p.isActivated()){
						powerUpsToDeactivate.add(p);
					}else if(p.isActivated()&&p.getTimeActivated()+timePoweredUp<System.nanoTime()/1000000){
						deactivatePowerUp(p.getTypeIndex());
						powerUpsToDeactivate.add(p);
					}
				}
				for(PowerUp p: powerUpsToDeactivate){
					powerUpsToRemove.remove(p);
				}
				powerUpsToDeactivate.clear();
				counterProjectile=0;
			}
			for(ExplosionAnimation ani: exploAnims){
				if(ani.tick(delta)>ani.getDuration()){
					exploAnimsToRemove.add(ani);
				}
			}
			for(ExplosionAnimation ani: exploAnimsToRemove){
				if(ani.tick(delta)>ani.getDuration()){
					exploAnims.remove(ani);
				}
			}
			exploAnimsToRemove.clear();
		}
	}
	
	public static int getScore(){
		return score;
	}

	@Override
	public int getID() {
		return 0;
	}
	
	/**
	 * Activates the power up effekt given by the power up type index.
	 * @param typeIndex
	 * @return
	 */
	public String activatePowerUp(int typeIndex){
		switch (typeIndex){
		case 0:
			// Double player rate of fire.
			timeBetweenPlayerFire/=1.5;
			return "Rapid fire";
		case 1:
			// Increase the barrels by one.
			playerBarrels++;
			return "Dubble barrel";
		
		case 2:
			// Increase health by one if not at max health.
			if(player.getHealth()<3){
				player.damage(-1);
			}
			return "";
		case 3:
			// Increase player-damage by one.
			player.changeDamage(1);
			return "Explosive bullets";
		default: 
			return "Error when generated random power up! Contact the gamecreators please!";
		}
	}
	
	/**
	 * Deactivate (reverse) power up with the given type index.
	 * @param typeIndex
	 */
	public void deactivatePowerUp(int typeIndex){
		switch (typeIndex){
		case 0:
			timeBetweenPlayerFire*=1.5;
			break;
		case 1:
			playerBarrels--;
			break;
		case 2:
			break;
		case 3:
			player.changeDamage(-1);
			break;
		}
	}
	
	/**
	 * Method taken from cyberspace, simply rounds to given digits.
	 * @param value
	 * @param places
	 * @return
	 */
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
