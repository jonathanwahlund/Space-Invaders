import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * @author Filip Reinholdsson, Jonathan Wahlund
 * Simply stores the game textures.
 */
public class Textures {
	
	// Image objects which will hold textures
		static Image background; 
		static Image playerTexture;
		static Image enemyTexture;
		static Image bossTexture;
		static Image bulletTexture;
		static Image bulletTextureFlip;
		static Image healthIcon;
		static Image emptyHealthIcon;
		static SpriteSheet explosion1ss;
		static Animation explosion1;
		static Image selection;
		static Image mainMenu;
		static Image gameOver;
	
	public static void initialize() throws SlickException{
		background = new Image("/res/bk1.jpg"); // Loads the background image
		playerTexture = new Image("/res/spaceships/medium/36.png"); // Loads the player texture
		enemyTexture = new Image("/res/spaceships/small/7.png").getFlippedCopy(true, true); // Loads the enemy texture
		bossTexture = new Image("/res/spaceships/small/8.png").getFlippedCopy(true, true); // Loads the boss texture
		bulletTexture = new Image("/res/bullet1.png"); // Loads the image for the player bullets
		bulletTextureFlip = new Image("/res/bullet1.png").getFlippedCopy(true, true); // Loads a flipped version of the image above for the enemy bullets
		healthIcon = new Image("/res/health.png");
		emptyHealthIcon=new Image ("/res/health_empty.png");
		explosion1ss = new SpriteSheet(new Image("/res/explosion2.png"), 32, 32);
		selection = new Image ("/res/selection.png");
		mainMenu = new Image ("/res/space2.png");
		gameOver = new Image ("/res/gameover.png");
	}

}
